# CMS-web testing

Projekt koji smo testirali predstavlja CMS sustav u vidu mini bloga. 
Poveznica na web stranicu : https://ribulb.hr/ruzica
Tehologije koje smo koristili u svrhu web testiranja su Katalon Studio, odnosno Katalon Recorder plugin, radi se zapravo o alatu koji je kompatibilna
zamjena Seleniuma koji ima mogućnost snimanja rada na stranici, debugiranja, pisanja automatiziranih testova te eksportiranja u python, C#,
Java, Robot framework datoteku.

Vezano za spomenuti projekt, izgenerirani su testovi za prijavu u sučelje za admina, registraciju, dodavanje i uređivanje postova, dodavanje
kategorija te odjavu.


U svrhu testiranja sigurnosti web stranice koristili smo OWASP ZAP alat. Spomenuti alat radi na principu otkrivanja svih url-ova koje sadrži 
web stranica te skeniranja istih. (report.html)


