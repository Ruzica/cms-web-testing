# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class EditingPost(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.katalon.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_editing_post(self):
        driver = self.driver
        driver.get("https://ribulb.hr/ruzica/admin/posts.php")
        driver.find_element_by_link_text("Uredi").click()
        driver.find_element_by_name("title").click()
        driver.find_element_by_name("title").clear()
        driver.find_element_by_name("title").send_keys("Post12")
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Autor'])[1]/following::input[1]").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Autor'])[1]/following::input[1]").clear()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Autor'])[1]/following::input[1]").send_keys("Autor12")
        driver.find_element_by_name("image").click()
        driver.find_element_by_name("image").clear()
        driver.find_element_by_name("image").send_keys("C:\\fakepath\\IMG_20180805_191832 - Copy.jpg")
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Autor'])[1]/following::input[1]").click()
        driver.find_element_by_name("post_content").click()
        driver.find_element_by_name("post_content").clear()
        driver.find_element_by_name("post_content").send_keys("Sadrzaj12")
        driver.find_element_by_name("post_tags").click()
        driver.find_element_by_name("post_tags").clear()
        driver.find_element_by_name("post_tags").send_keys("stobrec fotografija2")
        driver.find_element_by_name("update_post").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
